#!/bin/bash

# https://stackoverflow.com/a/17841619
function join_by { local d=${1-} f=${2-}; if shift 2; then printf %s "$f" "${@/#/$d}"; fi; }

libs=$(curl -s https://gitlab.com/libreumg/square2webapp/container/squarecontrolcontainer/-/raw/master/Dockerfile |
  awk -F\' '/library\(.*\)/ {print $2}' | sed 's/.*library(\(.*\)).*/\1/')

cmd="R --vanilla -e 'source(\"https://gitlab.com/libreumg/square2webapp/container/squarecontrolcontainer/-/raw/master/Rprofile.site\");' -e 'r <- getOption(\"repos\"); r[[\"CRAN\"]] <- NULL; options(repos=r)' -e 'update.packages(c(\"$(join_by '", "' $libs)\", repos = getOption(\"repos\"))); unname(apply(installed.packages()[, c(\"Package\", \"Version\")], 1, paste, collapse=\" \"))'"

echo $cmd"..."
eval $cmd

# note: does only work if the R session can access gitlab
